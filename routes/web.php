<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Main Menu
Route::get('/', 'HomeController@index');
Route::get('/about', 'AboutController@index');

// Katalog
Route::get('/jurnal', 'JurnalController@index');

// Katalog
Route::get('/persediaan', 'PersediaanController@index');

// COA
Route::get('/listcoa', 'CoaController@index');

// Data Transaksi
Route::get('/pembelian', 'PembelianController@index');
Route::get('/penjualan', 'PenjualanController@index');
Route::get('/tambahstok', 'TambahStokController@index');
Route::get('/transaksi', 'TransaksiController@index');

// Laporan
Route::get('/bukubesar', 'BukuBesarController@index');
Route::get('/nss', 'NssController@index');
Route::get('/phu', 'PhuController@index');
Route::get('/laporanneraca', 'LaporanNeracaController@index');