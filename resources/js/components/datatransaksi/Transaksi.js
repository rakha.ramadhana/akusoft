import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {
    Header,
    Form, 
    Container,
    Dropdown,
    Button
  } from 'semantic-ui-react'

const wrapper = {
    paddingTop: '15px',
    paddingBottom: '15px'
}

    const options = [
        { value: 'Pilihan 1', text: 'Pilihan 1' },
        { value: 'Pilihan 2', text: 'Pilihan 2' },
        { value: 'Pilihan 3', text: 'Pilihan 3' },
    ]

export default class Transaksi extends Component {
    state = {
        activeItem: 'home'
    }
    render() {
        return (
            <div>
                <Container text className="row" style={ wrapper }>
                    <Header as='h1'>Transaksi</Header>
                    <Form>
                        <Form.Field>
                        <label>Deskripsi Transaksi</label>
                        <input placeholder='Deskripsi Transaksi' />
                        </Form.Field>
                        <Form.Field>
                        <label>Tanggal</label>
                        <input placeholder='Tanggal' />
                        </Form.Field>
                        <Form.Field>
                        <label>Jumlah</label>
                        <input placeholder='0' />
                        </Form.Field>
                        <Form.Field>
                            <label>Akun Debit</label>
                            <Dropdown 
                            placeholder='Select...' 
                            selection
                            search
                            options={options}
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>Akun Kredit</label>
                            <Dropdown 
                            placeholder='Select...' 
                            selection
                            search
                            options={options}
                            />
                        </Form.Field>
                        <Button type='submit'>Submit</Button>
                    </Form>
                </Container>
            </div>
        );
    }
}

if (document.getElementById('transaksi')) {
    ReactDOM.render(<Transaksi />, document.getElementById('transaksi'));
}
