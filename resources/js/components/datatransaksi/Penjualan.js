import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {
    Header,
    Form, 
    Container,
    Dropdown,
    Button
  } from 'semantic-ui-react'

const wrapper = {
    paddingTop: '15px',
    paddingBottom: '15px'
}

const options = [
    { value: 'Pilihan 1', text: 'Pilihan 1' },
    { value: 'Pilihan 2', text: 'Pilihan 2' },
    { value: 'Pilihan 3', text: 'Pilihan 3' },
]

export default class Penjualan extends Component {
    state = {
        activeItem: 'home'
    }
    render() {
        return (
            <div>
                <Container text className="row" style={ wrapper }>
                    <Header as='h1'>Penjualan</Header>
                    <Form>
                        <Form.Field>
                        <label>Deskripsi Penjualan</label>
                        <input placeholder='Deskripsi Transaksi' />
                        </Form.Field>
                        <Form.Field>
                        <label>Tanggal</label>
                        <input placeholder='Tanggal' />
                        </Form.Field>
                        <Form.Field>
                        <label>Harga Jual</label>
                        <input placeholder='0' />
                        </Form.Field>
                        <Form.Field>
                        <label>Kuantitas</label>
                        <input placeholder='0' />
                        </Form.Field>
                        <Form.Field>
                            <label>Akun Debit</label>
                            <Dropdown 
                            placeholder='Select...' 
                            selection
                            search
                            options={options}
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>Akun Kredit</label>
                            <Dropdown 
                            placeholder='Select...' 
                            selection
                            search
                            options={options}
                            />
                        </Form.Field>
                        <Button type='submit'>Submit</Button>
                    </Form>
                </Container>
            </div>
        );
    }
}

if (document.getElementById('penjualan')) {
    ReactDOM.render(<Penjualan />, document.getElementById('penjualan'));
}
