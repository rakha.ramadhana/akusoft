import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {
    Header,
    Table,
    Menu,
    Icon, 
    Container,
    Label
  } from 'semantic-ui-react'

const wrapper = {
    paddingTop: '15px',
    paddingBottom: '15px'
}

export default class Jurnal extends Component {

    constructor(props){
        super(props)
        this.state={
            listjurnal:['']
        }
        console.log(super.constructor);
    }

    componentDidMount(){
        axios.get('/api/jurnal')
        .then(response=>{
            this.setState({listjurnal:response.data})
        })
    }


    render() {
        return (
            <div>
                <Container className="row" style={ wrapper }>
                    <Header as='h1'>Jurnal</Header>
                    <Table celled stackable>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>No</Table.HeaderCell>
                                <Table.HeaderCell>Transaksi</Table.HeaderCell>
                                <Table.HeaderCell>Jumlah</Table.HeaderCell>
                                <Table.HeaderCell>Akun Debit</Table.HeaderCell>
                                <Table.HeaderCell>Akun Kredit</Table.HeaderCell>
                                <Table.HeaderCell>Created At</Table.HeaderCell>
                                <Table.HeaderCell>Updated At</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {this.state.listjurnal.map((listjurnal, index)=>{
                                return(
                                    <Table.Row key={index}>
                                        <Table.Cell>{index + 1}</Table.Cell>
                                        <Table.Cell>{listjurnal.transaksi}</Table.Cell>
                                        <Table.Cell>{listjurnal.jumlah}</Table.Cell>
                                        <Table.Cell>{listjurnal.akun_debit}</Table.Cell>
                                        <Table.Cell>{listjurnal.akun_kredit}</Table.Cell>
                                        <Table.Cell>{listjurnal.created_at}</Table.Cell>
                                        <Table.Cell>{listjurnal.updated_at}</Table.Cell>
                                    </Table.Row>
                                    )
                                })
                            }
                        </Table.Body>

                        <Table.Footer>
                            <Table.Row>
                                <Table.HeaderCell colSpan='10'>
                                    <Menu floated='right' pagination>
                                        <Menu.Item as='a' icon>
                                        <Icon name='chevron left' />
                                        </Menu.Item>
                                        <Menu.Item as='a'>1</Menu.Item>
                                        <Menu.Item as='a'>2</Menu.Item>
                                        <Menu.Item as='a'>3</Menu.Item>
                                        <Menu.Item as='a'>4</Menu.Item>
                                        <Menu.Item as='a' icon>
                                        <Icon name='chevron right' />
                                        </Menu.Item>
                                    </Menu>
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Footer>
                    </Table>
                </Container>
            </div>
        );
    }
}

if (document.getElementById('jurnal')) {
    ReactDOM.render(<Jurnal />, document.getElementById('jurnal'));
}
