import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {
    Container,
    Grid,
    Header,
    Segment,
  } from 'semantic-ui-react'

export default class Footer extends Component {
    render() {
        return (
            <div>
                <Segment vertical inverted style={{ padding: '2em 0em' }}>
                <Container>
                    <Grid divided inverted stackable centered>
                        <p>
                            Copyright © 2018 AKUSOFT || R'Creative
                        </p>
                    </Grid>
                </Container>
                </Segment>
            </div>
        );
    }
}

if (document.getElementById('footer')) {
    ReactDOM.render(<Footer />, document.getElementById('footer'));
}
