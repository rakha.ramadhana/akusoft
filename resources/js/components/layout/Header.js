import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Icon, Menu, Input, Header, Image, Sidebar } from 'semantic-ui-react'

export default class HeaderTag extends Component {
    state = {visible: false}

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    handleHideClick = () => this.setState({ visible: false })
    handleShowClick = () => this.setState({ visible: true })
    handleSidebarHide = () => this.setState({ visible: false })

    render() {        
        const { activeItem } = this.state
        const { visible } = this.state

        return (
            <div>
                <Menu inverted attached>
                    <Menu.Item onClick={this.handleShowClick}>
                        <Icon name='bars' size='large' />
                    </Menu.Item>
                    <Header inverted as='h5' icon textAlign='center'>
                        <Image circular src='https://react.semantic-ui.com/logo.png' /> AKUSOFT
                    </Header>
                </Menu>

                <Sidebar as={Menu}
                    animation='overlay'
                    icon='labeled'
                    inverted vertical visible
                    onHide={this.handleSidebarHide}
                    visible={visible}
                    size='massive'
                    >
                    <Menu.Item>
                        <Image centered circular src='https://react.semantic-ui.com/images/avatar/large/patrick.png' size='tiny' />
                        <Header inverted size='tiny' textAlign='center'>Admin</Header>
                    </Menu.Item>
                    <Menu.Item>
                        <Input placeholder='Search...' />
                    </Menu.Item>

                    <Menu.Item>
                    Main Menu
                    <Menu.Menu>
                    <Menu.Item
                        name='home'
                        active={activeItem === 'home'}
                        href='/'
                        onClick={this.handleItemClick}
                        >
                        Home
                        </Menu.Item>
                        <Menu.Item
                        name='about'
                        active={activeItem === 'about'}
                        href='/about'
                        onClick={this.handleItemClick}
                        >
                        About
                        </Menu.Item>
                    </Menu.Menu>
                    </Menu.Item>

                    <Menu.Item
                        name='jurnal'
                        active={activeItem === 'jurnal'}
                        href='/jurnal'
                        onClick={this.handleItemClick}
                        >
                    Jurnal
                    </Menu.Item>

                    <Menu.Item>
                    Data Transaksi
                    <Menu.Menu>
                    <Menu.Item
                        name='transaksi'
                        active={activeItem === 'transaksi'}
                        href='/transaksi'
                        onClick={this.handleItemClick}
                        >
                        Transaksi
                        </Menu.Item>
                        <Menu.Item
                        name='pembelian'
                        active={activeItem === 'pembelian'}
                        href='/pembelian'
                        onClick={this.handleItemClick}
                        >
                        Pembelian
                        </Menu.Item>
                        <Menu.Item
                        name='penjualan'
                        active={activeItem === 'penjualan'}
                        href='/penjualan'
                        onClick={this.handleItemClick}
                        >
                        Penjualan
                        </Menu.Item>
                        <Menu.Item
                        name='tambahstok'
                        active={activeItem === 'tambahstok'}
                        href='/tambahstok'
                        onClick={this.handleItemClick}
                        >
                        Tambah Stok
                        </Menu.Item>
                    </Menu.Menu>
                    </Menu.Item>

                    <Menu.Item 
                        name='listcoa'
                        active={activeItem === 'listcoa'}
                        href='/listcoa'
                        onClick={this.handleItemClick}
                        >
                    Chart of Account
                    </Menu.Item>

                    <Menu.Item
                        name='persediaan'
                        active={activeItem === 'persediaan'}
                        href='/persediaan'
                        onClick={this.handleItemClick}
                        >
                    Persediaan
                    </Menu.Item>

                    <Menu.Item>
                    Laporan
                        <Menu.Menu>
                            <Menu.Item
                                name='bukubesar'
                                active={activeItem === 'bukubesar'}
                                href='/bukubesar'
                                onClick={this.handleItemClick}
                                >
                                Buku Besar
                            </Menu.Item>
                            <Menu.Item
                                name='nss'
                                active={activeItem === 'nss'}
                                href='/nss'
                                onClick={this.handleItemClick}
                                >
                                Neraca Saldo Sementara (NSS)
                            </Menu.Item>
                            <Menu.Item
                                name='phu'
                                active={activeItem === 'phu'}
                                href='/phu'
                                onClick={this.handleItemClick}
                                >
                                Perhitungan Hasil Usaha (PHU)
                            </Menu.Item>
                            <Menu.Item
                                name='laporanneraca'
                                active={activeItem === 'laporanneraca'}
                                href='/laporanneraca'
                                onClick={this.handleItemClick}
                                >
                                Laporan Neraca
                            </Menu.Item>
                        </Menu.Menu>
                    </Menu.Item>
                </Sidebar>
            </div>
        );
    }
}

if (document.getElementById('header')) {
    ReactDOM.render(<HeaderTag />, document.getElementById('header'));
}
