import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {
    Header,
    Image, 
    Container,
    Table, 
    Label,
    Menu,
    Icon,
  } from 'semantic-ui-react';
import axios from 'axios';

const wrapper = {
    paddingTop: '15px',
    paddingBottom: '15px'
}

export default class ListCOA extends Component {

    constructor(props){
        super(props);
        this.state={
            listcoa:['']
        }
        console.log(super.constructor);
    }

    componentDidMount(){
        axios.get('/api/listcoa')
        .then(response=>{
            this.setState({listcoa:response.data})
        })
    }

    render() {
        return (
            <div>
                <Container className="row" style={ wrapper }>
                    <Header as='h1'>List COA</Header>
                    <Table celled stackable>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>No</Table.HeaderCell>
                                <Table.HeaderCell>Kode</Table.HeaderCell>
                                <Table.HeaderCell>Nama Akun</Table.HeaderCell>
                                <Table.HeaderCell>Keterangan</Table.HeaderCell>
                                <Table.HeaderCell>Saldo Awal</Table.HeaderCell>
                                <Table.HeaderCell>Saldo</Table.HeaderCell>
                                <Table.HeaderCell>Created At</Table.HeaderCell>
                                <Table.HeaderCell>Updated At</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {this.state.listcoa.map((listcoa, index)=>{
                                return(
                                    <Table.Row key={index}>
                                        <Table.Cell>{index + 1}</Table.Cell>
                                        <Table.Cell>{listcoa.kode}</Table.Cell>
                                        <Table.Cell>{listcoa.nama_akun}</Table.Cell>
                                        <Table.Cell>{listcoa.keterangan}</Table.Cell>
                                        <Table.Cell>{listcoa.saldo_awal}</Table.Cell>
                                        <Table.Cell>{listcoa.saldo}</Table.Cell>
                                        <Table.Cell>{listcoa.created_at}</Table.Cell>
                                        <Table.Cell>{listcoa.updated_at}</Table.Cell>
                                    </Table.Row>
                                    )
                                })
                            }
                        </Table.Body>

                        <Table.Footer>
                            <Table.Row>
                                <Table.HeaderCell colSpan='10'>
                                    <Menu floated='right' pagination>
                                        <Menu.Item as='a' icon>
                                        <Icon name='chevron left' />
                                        </Menu.Item>
                                        <Menu.Item as='a'>1</Menu.Item>
                                        <Menu.Item as='a'>2</Menu.Item>
                                        <Menu.Item as='a'>3</Menu.Item>
                                        <Menu.Item as='a'>4</Menu.Item>
                                        <Menu.Item as='a' icon>
                                        <Icon name='chevron right' />
                                        </Menu.Item>
                                    </Menu>
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Footer>
                    </Table>
                </Container>
            </div>
        );
    }
}

if (document.getElementById('listcoa')) {
    ReactDOM.render(<ListCOA />, document.getElementById('listcoa'));
}
