import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {
    Header,
    Table,
    Menu,
    Icon, 
    Container
  } from 'semantic-ui-react'

const wrapper = {
    paddingTop: '15px',
    paddingBottom: '15px'
}

export default class Persediaan extends Component {

    constructor(props){
        super(props)
        this.state={
            listpersediaan:['']
        }
        console.log(super.constructor);
    }

    componentDidMount(){
        axios.get('/api/persediaan')
        .then(response=>{
            this.setState({listpersediaan:response.data})
        })
    }


    render() {
        return (
            <div>
                <Container className="row" style={ wrapper }>
                    <Header as='h1'>Persediaan</Header>
                    <Table celled stackable>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>No</Table.HeaderCell>
                                <Table.HeaderCell>Nama Barang</Table.HeaderCell>
                                <Table.HeaderCell>Kuantitas</Table.HeaderCell>
                                <Table.HeaderCell>Jenis Barang</Table.HeaderCell>
                                <Table.HeaderCell>Harga Beli</Table.HeaderCell>
                                <Table.HeaderCell>Harga Jual</Table.HeaderCell>
                                <Table.HeaderCell>Harga Pokok</Table.HeaderCell>
                                <Table.HeaderCell>Supplier</Table.HeaderCell>
                                <Table.HeaderCell>Created At</Table.HeaderCell>
                                <Table.HeaderCell>Updated At</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {this.state.listpersediaan.map((listpersediaan, index)=>{
                                return(
                                    <Table.Row key={index}>
                                        <Table.Cell>{index + 1}</Table.Cell>
                                        <Table.Cell>{listpersediaan.nama_barang}</Table.Cell>
                                        <Table.Cell>{listpersediaan.kuantitas}</Table.Cell>
                                        <Table.Cell>{listpersediaan.jenis_barang}</Table.Cell>
                                        <Table.Cell>{listpersediaan.harga_beli}</Table.Cell>
                                        <Table.Cell>{listpersediaan.harga_jual}</Table.Cell>
                                        <Table.Cell>{listpersediaan.harga_pokok}</Table.Cell>
                                        <Table.Cell>{listpersediaan.supplier}</Table.Cell>
                                        <Table.Cell>{listpersediaan.created_at}</Table.Cell>
                                        <Table.Cell>{listpersediaan.updated_at}</Table.Cell>
                                    </Table.Row>
                                    )
                                })
                            }
                        </Table.Body>

                        <Table.Footer>
                            <Table.Row>
                                <Table.HeaderCell colSpan='10'>
                                    <Menu floated='right' pagination>
                                        <Menu.Item as='a' icon>
                                        <Icon name='chevron left' />
                                        </Menu.Item>
                                        <Menu.Item as='a'>1</Menu.Item>
                                        <Menu.Item as='a'>2</Menu.Item>
                                        <Menu.Item as='a'>3</Menu.Item>
                                        <Menu.Item as='a'>4</Menu.Item>
                                        <Menu.Item as='a' icon>
                                        <Icon name='chevron right' />
                                        </Menu.Item>
                                    </Menu>
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Footer>
                    </Table>
                </Container>
            </div>
        );
    }
}

if (document.getElementById('persediaan')) {
    ReactDOM.render(<Persediaan />, document.getElementById('persediaan'));
}
