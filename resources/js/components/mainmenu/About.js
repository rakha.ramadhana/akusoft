import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {
    Header,
    Image, 
    Container
  } from 'semantic-ui-react'

const wrapper = {
    paddingTop: '15px',
    paddingBottom: '15px'
}

export default class About extends Component {
    state = {
        activeItem: 'home'
    }
    render() {
        return (
            <div>
                <Container text className="row" style={ wrapper }>
                    <Header as='h1'>About</Header>
                    <p>
                    Kami merupakan...
                    </p>
                    <p>
                    Kami membuat aplikasi dalam rangka untuk...
                    </p>
                </Container>
            </div>
        );
    }
}

if (document.getElementById('about')) {
    ReactDOM.render(<About />, document.getElementById('about'));
}
