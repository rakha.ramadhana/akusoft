import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {
    Header,
    Image, 
    Container
  } from 'semantic-ui-react'

const wrapper = {
    paddingTop: '15px',
    paddingBottom: '15px'
}

export default class Home extends Component {
    render() {
        return (
            <div className="row" style={ wrapper }>
                <Container text>
                    <Header as='h1'>Welcome to AKUSOFT</Header>
                    <p>
                    Selamat datang di Akusoft.
                    </p>
                    <p>
                    Akusoft dapat melakukan hal-hal dibawah ini:
                    </p>
                </Container>
            </div>
        );
    }
}

if (document.getElementById('home')) {
    ReactDOM.render(<Home />, document.getElementById('home'));
}
