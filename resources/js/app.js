
import 'semantic-ui-css/semantic.min.css'

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Layout
require('./components/layout/Header');
require('./components/layout/Footer');

// Main Menu
require('./components/mainmenu/Home');
require('./components/mainmenu/About');

// Jurnal
require('./components/jurnal/Jurnal');

// Katalog
require('./components/katalog/Persediaan');

// COA
require('./components/chartofaccount/ListCoa');

// Data Transaksi
require('./components/datatransaksi/Pembelian');
require('./components/datatransaksi/Penjualan');
require('./components/datatransaksi/TambahStok');
require('./components/datatransaksi/Transaksi');

// Laporan
require('./components/laporan/BukuBesar');
require('./components/laporan/NSS');
require('./components/laporan/PHU');
require('./components/laporan/LaporanNeraca');