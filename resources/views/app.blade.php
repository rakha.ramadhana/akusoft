<!-- Stored in resources/views/layouts/app.blade.php -->

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('layout.include.head')
    </head>
    <body>
        <header class="row">
            @include('layout.partial.header')
        </header>
        
        <div id="main" class="row">
            @yield('content')
        </div>

        <footer style="position: relative; left:0; bottom:0; right:0; width: 100%">
            @include('layout.partial.footer')
        </footer>

        <script src='/js/app.js'></script>
    </body>
</html>