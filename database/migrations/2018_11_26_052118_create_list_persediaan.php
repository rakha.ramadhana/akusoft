<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListPersediaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_persediaan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_barang');
            $table->integer('kuantitas');
            $table->string('jenis_barang');
            $table->integer('harga_beli');
            $table->integer('harga_jual');
            $table->integer('harga_pokok');
            $table->string('supplier');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_persediaan');
    }
}
