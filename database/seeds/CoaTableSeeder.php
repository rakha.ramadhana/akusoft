<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CoaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach (range(1, 5) as $index)
        {
            DB::table('list_coa')->insert([
                'kode' => str_random(3),
                'nama_akun' => str_random(10),
                'keterangan' => 'Akun',
                'saldo_awal' => rand(10000,1000000),
                'saldo' => rand(10000,1000000),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
