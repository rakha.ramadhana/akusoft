<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class JurnalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach (range(1, 5) as $index)
        {
            DB::table('list_jurnal')->insert([
                'transaksi' => str_random(10),
                'jumlah' => rand(10000,1000000),
                'akun_debit' => str_random(10),
                'akun_kredit' => str_random(10),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
