<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PersediaanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach (range(1, 5) as $index)
        {
            DB::table('list_persediaan')->insert([
                'nama_barang' => str_random(10),
                'kuantitas' => rand(1,100),
                'jenis_barang' => str_random(10),
                'harga_beli' => rand(1000,100000),
                'harga_jual' => rand(1000,100000),
                'harga_pokok' => rand(1000,100000),
                'supplier' => str_random(5),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
