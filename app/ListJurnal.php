<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListJurnal extends Model
{
    public $table = "list_jurnal";

    protected $fillable = [
        'transaksi', 'jumlah', 'akun_debit', 'akun_kredit'
    ];
}
