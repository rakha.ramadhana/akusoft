<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListCOA extends Model
{
    public $table = "list_coa";

    protected $fillable = [
        'kode','nama_akun', 'keterangan', 'saldo_awal'
    ];
}