<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    // Redirect to resources/views/home.blade.php
    public function index() {
        return view('mainmenu/home');
    }
}
