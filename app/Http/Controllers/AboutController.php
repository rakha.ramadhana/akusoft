<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    // Redirect to resources/views/about.blade.php
    public function index() {
        return view('mainmenu/about');
    }
}
