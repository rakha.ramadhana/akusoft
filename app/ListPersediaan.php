<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListPersediaan extends Model
{
    public $table = "list_persediaan";

    protected $fillable = [
        'nama_barang', 'kuantitas', 'jenis_barang', 'harga_beli', 'supplier'
    ];
}